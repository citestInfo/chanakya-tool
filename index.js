var fs = require('fs')
var cors = require('cors')
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

// to parse the nested object
app.use(bodyParser.urlencoded({
    extended: true
}));

// to allow cross origin resource sharing 
app.options('*', cors())

// route definition
// readfile and append object into the test json
app.post('/myaction', function (req, res) {
            fs.readFile('results.json', function (err, data) {
                var json = JSON.parse(data);
                json.push(req.body);
                fs.writeFile("results.json", JSON.stringify(json), function (err) {
                    if (err) throw err;
                    console.log('The "data to append" was appended to file!');
                });
            })
});
// server with port  no 8080
app.listen(8080, function () {
    console.log('Server running at http://127.0.0.1:8080/');
});
